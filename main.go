package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func initBoard(width int, height int, goal int) *board {
	var BOARD board
	BOARD.width = width
	BOARD.height = height
	BOARD.goal = goal
	BOARD.generateBoard()

	max := 2
	for max > 0 {
		BOARD.spawnNumber()
		max--
	}

	return &BOARD
}

func checkWin(b *board) {
	if b.isWin == true {
		fmt.Println("You WIN!!! Goal: ", b.goal)
	}
}

func checkLose(b *board) {
	if b.isLose == true {
		fmt.Println("You LOSE!!! Can't spawn new number")
	}
}

func playGame(b *board) {
	for b.isGameOver == false {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Input direction (a/s/w/d): ")
		direction, _ := reader.ReadString('\n')
		direction = strings.TrimSuffix(direction, "\n")

		switch strings.ToLower(direction) {
		case "a":
			b.moveWest()
		case "d":
			b.moveEast()
		case "w":
			b.moveNorth()
		case "s":
			b.moveSouth()
		}

		b.checkGameOver()
		fmt.Println("Goal: ", b.goal)
		fmt.Println("Score: ", b.score)
		// fmt.Println("Max number: ", b.maxNumber)
		// fmt.Println("Win? ", b.isWin)
		// fmt.Println("Gameover? ", b.isGameOver)
		fmt.Println(b.renderBoard())
	}

	checkWin(b)
	checkLose(b)
}

func main() {
	BOARD := initBoard(4, 4, 2048)

	fmt.Println(BOARD.renderBoard())
	playGame(BOARD)
}
